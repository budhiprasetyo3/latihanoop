<?php
require_once('animal1.php');

class frog extends animal
{
    public $legs = 4;
    public $cooldblooded = "no";   
    public $jump = "jump jump";
    
    public function yell()
    {
        echo "auoo uooo";
    }
}