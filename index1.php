<?php

require('animal1.php');
require('ape.php');
require('frog.php');
$object = new animal("wolf");

echo "Nama Hewan : $object->name <br>";
echo "Jumlah Kaki : $object->legs <br>";
echo "Berdarah dingin : $object->coldblooded <br><br>";

$object1 = new ape ("kera sakti");

echo "Nama Hewan : $object1->name <br>";
echo "Jumlah kaki : $object1->legs <br>";
echo "Berdarah dingin : $object1->cooldblooded <br>";
echo "yell : $object1->yell() <br> <br>";

$object2 = new frog ("budug");

echo "Nama Hewan : $object2->name <br>";
echo "Jumlah kaki : $object2->legs <br>";
echo "Berdarah dingin : $object2->cooldblooded <br>";
echo "jump : $object2->jump()";

?>